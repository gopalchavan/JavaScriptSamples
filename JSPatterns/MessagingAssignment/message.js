

(function () {
    var Subject = function () {
        var observers = [];

        return {
            subscribeObserver: function (observer) {
                observers.push(observer);
            },
            unsubscribeObserver: function (observer) {
                var index = observers.indexOf(observer);
                if (index > -1) {
                    observers.splice(index, 1);
                }
            },
            notifyObserver: function (observer, message) {
                var index = observers.indexOf(observer);
                if (index > -1) {
                    observers[index].notify(message);
                }
            },
            notifyAllObservers: function (message) {
                for (var i = 0; i < observers.length; i++) {
                    observers[i].notify(message);
                };
            }
        };
    }

    var Observer = function (element) {
        var _selected = false;
        var _messageBox = element;
        return {
            notify: function (message) {
                var msg = document.createElement('span');
                msg.textContent = message;
                _messageBox.appendChild(msg);
                _messageBox.appendChild(document.createElement('br'));
            },
            selected: _selected
        }
    }

    var subject = new Subject();

    $("#addButton").click(function (e) {
        var messageBox = document.createElement("div");
        messageBox.classList.add("msgBox");
        var subscribe = document.createElement("button");
        subscribe.classList.add("btn");
        subscribe.classList.add("btn-default");
        subscribe.textContent = 'click to subscribe';
        messageBox.appendChild(subscribe);
        var observer = new Observer(messageBox);
        $("#messageBoxes").append(messageBox);

        subscribe.onclick = function (e) {
            if (!observer.selected) {
                e.target.textContent = 'click to unsubscribe';
                observer.selected = true;
                subject.subscribeObserver(observer);
            }
            else {
                e.target.textContent = 'click to subscribe';
                observer.selected = false;
                subject.unsubscribeObserver(observer);
            }
        };
    });

    $("#send").click(function (e) {
        subject.notifyAllObservers($('#message').val());
        $('#message').val('');
    });
})()