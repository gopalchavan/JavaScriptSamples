var firstNameElement = document.getElementById("firstname");
var lastNameElement = document.getElementById("lastname");
var EstablishedYearElement = document.getElementById("establishedyear");
var ceoElement = document.getElementById("ceo");
var optionElement = document.getElementById("option");
var sendButton = document.getElementById("sendData");
var dataElement = document.getElementById("data");

var details = function (firstName, lastName, EstablishedYear, ceo) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.EstablishedYear = EstablishedYear;
    this.ceo = ceo;
}

details.prototype = {
    api1Payload: function () {
        var payLoad = {
            Company: {
                FirstName: this.firstName,
                LastName: this.lastName,
                Details: {
                    EstablishedYear: this.EstablishedYear,
                    CEO: this.ceo
                }
            }
        };
        return payLoad;
    },
    api2Payload: function () {
        var payLoad = {
            Company: {
                Name: {
                    FirstName: this.firstName,
                    LastName: this.lastName,
                }
            },
            EstablishedYear: this.EstablishedYear,
            CEO: this.ceo
        }
        return payLoad;
    }
}

sendButton.onclick = function () {
    var defaultObj = new details(firstNameElement.value, lastNameElement.value, EstablishedYearElement.value, ceoElement.value);

    if (optionElement.value == 1) {
        dataElement.textContent = JSON.stringify(defaultObj.api1Payload());
    }
    else {
        dataElement.textContent = JSON.stringify(defaultObj.api2Payload());
    }
}
