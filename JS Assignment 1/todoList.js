(function () {

    // Cosure for manuplating todoList array operation's
    var list = (function list() {
        var todoList = [];
        function set(id, name) {
            todoList.push(new task_obj_fun(id, name));
        }
        function get(id) {
            return todoList.find(elem => elem.Id == id);
        }
        function setStatus(id, status) {
            var res = get(id);
            if (res) {
                let index = todoList.findIndex(elem => elem.Id == id);
                todoList[index].status = status;
            }
        }
        function getStatus(id) {
            var res = get(id);
            if (res) {
                return todoList.find(elem => elem.Id == id).status;
            }
        }
        function remove_elem(id) {
            var res = get(id);
            if (res) {
                let index = todoList.findIndex(elem => elem.Id == id);
                todoList.splice(index, 1);
            }
        }
        function task_obj_fun(id, name) {
            return { Id: id, Name: name, status: false };
        }
        function getPercentage() {
            if (todoList.length == 0) {
                return 0;
            }
            else {
                let count = 0;
                for (let i = 0; i < todoList.length; i++) {
                    if (todoList[i].status == true) {
                        count++;
                    }
                }
                return count * 100 / todoList.length;
            }
        }
        return {
            set,
            get,
            setStatus,
            getStatus,
            remove_elem,
            getPercentage
        }
    })();

    // Closure for generating unique id's
    var uniqueId = (function increment() {
        var u_id = 1;
        return function incrementID() {
            return u_id++;
        }
    })();

    // Adding EventListener's to dom Elements
    document.getElementById("task_name").addEventListener("keyup", Validate);
    document.getElementById("add_task").addEventListener("click", AddTask)

    // Method for Task Name Validation 
    function Validate() {
        let taskName = document.getElementById("task_name").value;
        let error = document.getElementById("task_name_error");
        if (taskName.length < 4) {
            error.innerHTML = "Task Name must be of minimum 4 char";
            error.style.display = "block";
        }
        else if (taskName.length > 10) {
            error.innerHTML = "Task Name can't be more than 10 char";
            error.style.display = "block";
        }
        else {
            error.innerHTML = "";
            error.style.display = "none";
        }
    }
    // Method for adding Task's
    function AddTask() {
        let task = document.getElementById("task_name").value;
        if (task.length == 0) {
            alert("Task Name must be provided");
        }
        else if (task.length < 4) {
            alert("The Min len of Task Name must be 4 char");
        }
        else {
            let id = uniqueId();
            list.set(id, task);
            let taskObject = list.get(id);

            let pending_parent = document.getElementById("pending_body");
            let pending_child = document.createElement("div");
            pending_child.setAttribute("id", taskObject.Id);
            pending_child.setAttribute("class", "task");

            pending_child.innerHTML = `
                    <input class="chk_box" type="checkbox" id="cb_${taskObject.Id}">
                    <span class="task_nm">${taskObject.Name}</span>
                    <div class="close_icon" id="rm_${taskObject.Id}">&times</div>
            `

            pending_parent.appendChild(pending_child);
            document.getElementById(`cb_${taskObject.Id}`).addEventListener("click", check);
            document.getElementById(`rm_${taskObject.Id}`).addEventListener("click", remove);

        }
        document.getElementById("task_name").value = "";
        Calc_Progress();
    }

    // Method for Checking and unchecking checkbox
    function check(event) {
        console.log(event);
        let cb_id = event.target.id;
        let div_id = event.target.id.slice(3);
        let isChecked = document.getElementById(cb_id).checked;

        if (isChecked) {
            list.setStatus(div_id, true);
            let pending_parent = document.getElementById("pending_body");
            let pending_child = document.getElementById(div_id);

            let completed_parent = document.getElementById("completed_body");

            completed_parent.appendChild(pending_child);
        }
        else {
            list.setStatus(div_id, false);
            let pending_parent = document.getElementById("pending_body");

            let completed_parent = document.getElementById("completed_body");
            let completed_child = document.getElementById(div_id);

            pending_parent.appendChild(completed_child);
        }
        Calc_Progress();
    }

    // Method for Removing particular task
    function remove(event) {
        let rm_task_id = event.target.id.slice(3);
        let parent;
        let status = list.getStatus(rm_task_id);
        let parent_name = "pending_body";


        if (status == true) {
            parent_name = "completed_body";
        }
        parent = document.getElementById(parent_name);
        child = document.getElementById(rm_task_id);

        parent.removeChild(child);
        list.remove_elem(rm_task_id);
        Calc_Progress();
    }

    // Method for manuplating progress bar
    function Calc_Progress() {

        let percentage = list.getPercentage();
        let progress = document.getElementById("progress");
        let progress_text = document.getElementById("progress_text");

        if (percentage > 95) {
            progress.style.borderRadius = '20px';
        }
        else {
            progress.style.borderRadius = "0px";
            progress.style.borderTopLeftRadius = '20px';
            progress.style.borderBottomLeftRadius = '20px';
        }
        progress_text.innerHTML = `${percentage.toString().slice(0, 5)}%`;
        progress.style.width = `${percentage}%`;
    }
})();