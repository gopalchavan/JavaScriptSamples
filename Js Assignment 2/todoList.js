
// Cosure for manuplating todoList array operation's
const list = (function list() {
    const todoList = [];
    function getAll() {
        return todoList;
    }
    function set(id, name, status, tags) {
        todoList.push(new task_obj_fun(id, name, status, tags));
    }
    function get(id) {
        return todoList.find(elem => elem.Id == id);
    }
    function setStatus(id, status) {
        var res = get(id);
        if (res) {
            let index = todoList.findIndex(elem => elem.Id == id);
            todoList[index].Status = status;
        }
    }
    function getStatus(id) {
        var res = get(id);
        if (res) {
            return todoList.find(elem => elem.Id == id).status;
        }
    }
    function remove_elem(id) {
        var res = get(id);
        if (res) {
            let index = todoList.findIndex(elem => elem.Id == id);
            todoList.splice(index, 1);
        }
    }
    function task_obj_fun(id, name, status, tags) {
        return { Id: id, Name: name, Status: status, Tags: tags };
    }
    function getPercentage() {
        return todoList.length?getCompleted()*100/todoList.length:0;
    }
    function getCompleted(){
        if(todoList.length==0)
        {
            return 0;            
        }
        else{
            let count=0;
            todoList.forEach(elem=>elem.Status=='Completed'?count++:count);
            return count;
        }
    }
    function getPending(){
        return getAll().length-getCompleted();
    }
    return {
        set,
        get,
        getAll,
        setStatus,
        getStatus,
        remove_elem,
        getPercentage,
        getPending,
        getCompleted
    }
})();


// Closure for generating unique id's
const uniqueId = (function increment() {
    var u_id = 1;
    return function incrementID() {
        return u_id++;
    }
})();


//Close for manuplating tag's
const Tags_array = (function tags() {
    let tag_arr = [];
    function add(id, value) {
        tag_arr.push(new obj(id, value));
    }
    function remove(id) {
        let index = tag_arr.findIndex(elem => elem.Id == id);
        tag_arr.splice(index, 1);
    }
    function pop() {
        var res = tag_arr
        tag_arr = [];
        return res;
    }
    function obj(id, value) {
        return { Id: id, Value: value };
    }
    return {
        add,
        pop,
        remove
    }
})();


document.getElementById('show_more_btn').addEventListener('click', showMore)

function showMore() {
    var m = document.getElementById('progress_bar').style.display;
    if (m) {

        document.getElementById('progress_bar').style.display = '';
        document.getElementById('show_more_btn').innerHTML = '<b>+</b>'
        document.getElementById('completed').style.display = '';
        document.getElementById('pending').style.display = '';
    }
    else {
        document.getElementById('progress_bar').style.display = 'block';
        document.getElementById('show_more_btn').innerHTML = '<b>-</b>'
        document.getElementById('completed').style.display = 'block';
        document.getElementById('pending').style.display = 'block';
    }
}

// Method for manuplating progress bar
function Calc_Progress() {

    let percentage = list.getPercentage();
    let progress = document.getElementById("progress");
    let progress_text = document.getElementById("progress_text");

    if (percentage > 95) {
        progress.style.borderRadius = '20px';
    }
    else {
        progress.style.borderRadius = "0px";
        progress.style.borderTopLeftRadius = '20px';
        progress.style.borderBottomLeftRadius = '20px';
    }
    progress_text.innerHTML = `${percentage.toString().slice(0, 5)}%`;
    progress.style.width = `${percentage}%`;
    document.getElementById("pending").textContent=`Pending:${list.getPending()}`;
    document.getElementById("completed").textContent=`Completed:${list.getCompleted()}`;
}

// Get the modal
var modal = document.getElementById('myModal');
// Get the button that opens the modal
var btn = document.getElementById("add_task");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
// When the user clicks the button, open the modal 

btn.onclick = function () {
    modal.style.display = "block";
    document.getElementById('add_or_edit').value = 'Add Task';
    document.getElementById('add_or_edit').textContent = '';
}
// When the user clicks on <span> (x), close the modal
function cls() {
    modal.style.display = "none";
    document.getElementById('add_or_edit').value = 'Add Task';
    document.getElementById('add_or_edit').textContent = '';
}
span.onclick = function () {
    modal.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

function AddTask() {

    console.log("add task");
    let task_id;
    // Input data
    if (document.getElementById('add_or_edit').textContent) {
        task_id = parseInt(document.getElementById('add_or_edit').textContent, 10);
        list.remove_elem(task_id);
        document.getElementById(`tr_${task_id}`).remove();
    }
    else {
        task_id = uniqueId();
    }
    let task_name = document.getElementById('task_name').value;
    let task_status = document.getElementById('task_status').value;
    let task_tags = Tags_array.pop();
    let arr = [];
    for (let i = 0; i < task_tags.length; i++) {
        arr[i] = task_tags[i].Value;
    }

    // Adding to Array
    list.set(task_id, task_name, task_status, task_tags);

    // Hidding Modal popup
    modal.style.display = "none";
    Create_row(task_id, task_name, task_status, arr);


    document.getElementById('add_or_edit').value = 'Add Task';
    document.getElementById('add_or_edit').textContent = '';
    document.getElementById('task_name').value = '';
    document.getElementById('tags_field').innerHTML = '';
    document.getElementById('task_tag').value = '';

    console.log(list.getAll());
    Calc_Progress()
}
function Create_row(task_id, task_name, task_status, arr) {
    // Creating Row
    let row = document.createElement('tr');
    row.setAttribute('id', `tr_${task_id}`);


    // Creating column for check box
    var td_check = document.createElement('td');
    var cb = document.createElement('input');
    if (task_status == 'Completed') { cb.setAttribute('checked', true); }
    cb.setAttribute('type', 'checkbox');
    cb.onclick = function () { check(event, task_id) }
    td_check.appendChild(cb);

    // Creating column for Task Name
    var td_name = document.createElement('td');
    td_name.textContent = task_name;

    // Creating column for Task Status
    var td_status = document.createElement('td');
    td_status.textContent = task_status;

    // Creating column for Task tags
    var td_tags = document.createElement('td');
    td_tags.textContent = arr;


    // Creating column for Task Acitons
    var td_action = document.createElement('td');
    var btn_edit = document.createElement('button');
    btn_edit.textContent = "Edit";
    btn_edit.onclick = function () { edit(task_id) }
    var btn_remove = document.createElement('button');
    btn_remove.textContent = "Remove";
    btn_remove.onclick = function () { remove_task(task_id) }
    td_action.appendChild(btn_edit);
    td_action.appendChild(btn_remove);

    // Appending all td's to row
    row.appendChild(td_check);
    row.appendChild(td_name);
    row.appendChild(td_tags);
    row.appendChild(td_status);
    row.appendChild(td_action);


    document.getElementById('table_body').appendChild(row);
}
function check(event, id) {
    let status = event.target.checked ? 'Completed' : 'Pending';
    list.setStatus(id, status);
    console.log(list.get(id));
    document.getElementById(`tr_${id}`).getElementsByTagName('td')[3].textContent = status;
    Calc_Progress();
}
function edit(id) {
    document.getElementById('tags_field').innerHTML = '';
    Tags_array.pop();
    document.getElementById('add_or_edit').value = 'Save';
    document.getElementById('add_or_edit').textContent = id;
    // console.log(document.getElementById('add_or_edit'));
    // console.log(document.getElementById('add_or_edit').textContent);
    let obj = list.get(id);
    document.getElementById('task_name').value = obj.Name;
    document.getElementById('task_status').value = obj.Status;
    console.log(obj);
    var tagarr = obj.Tags;
    for (let i = 0; i < tagarr.length; i++) {
        // Tags_array.add(tagarr[i].Id,tagarr[i].Value);     
        addTag(tagarr[i].Id, tagarr[i].Value);
    }

    modal.style.display = "block";
    Calc_Progress();
}
function remove_task(id) {
    console.log(list.get(id));
    list.remove_elem(id);
    console.log(list.get(id));
    document.getElementById(`tr_${id}`).remove();
    Calc_Progress();
}

// Add Task to Form
document.getElementById('add_tag').addEventListener('click', addTag);
function addTag(OldId, OldVal) {
    let id;
    var tag_name;
    if (typeof (OldId) == 'number') {
        id = OldId;
        tag_name = OldVal;
    }
    else {
        id = uniqueId();
        tag_name = document.getElementById('task_tag').value;
    }
    let tag_div = document.createElement('div');
    tag_div.setAttribute('id', `tag_div_${id}`);



    Tags_array.add(id, tag_name);

    tag_div.innerHTML = `<b style="font-size:25px">${tag_name}</b><input type="button" style="color:#ff8888;font-size:25px;width:30px;" id='rm_tag_${id}' value='x'>`;
    document.getElementById('tags_field').appendChild(tag_div);
    document.getElementById(`rm_tag_${id}`).addEventListener('click', rm_tag);
    document.getElementById('task_tag').value = "";
}


function rm_tag(event) {
    var id = event.target.id + "";
    id = id[id.length - 1];
    Tags_array.remove(id);
    document.getElementById(`tag_div_${id}`).remove();
}
function remove_completed() {
    let temp=list.getAll().slice();
    console.log(temp);
    temp.forEach(task => {
        console.log(task);
        if (task.Status == "Completed") {
            document.getElementById(`tr_${task.Id}`).remove();
            list.remove_elem(task.Id);
        }
    });
    Calc_Progress();
}
function showAllPendingCompleted(event) {
    let val = event.target.value;
    if (val == "All") {
        refresh_Tbl_Body();
        list.getAll().forEach(elem => {            
            Create_row(elem.Id, elem.Name, elem.Status, getTagsValue(elem.Tags));
        });

    }
    else{
        refresh_Tbl_Body();
        list.getAll().forEach(elem => {
            if(elem.Status==val)
            {
                Create_row(elem.Id, elem.Name, elem.Status,getTagsValue(elem.Tags));
            }            
        });
    }
}
function refresh_Tbl_Body() {
    document.getElementById('table_body').remove();

    var bdy = document.createElement('tbody');
    bdy.setAttribute('id', 'table_body');
    document.getElementById('tsk_tbl').appendChild(bdy);
}
function getTagsValue(tag_arr) {
    let arr = [];
    for (let i = 0; i < tag_arr.length; i++) {
        arr[i] = tag_arr[i].Value;
    }
    return arr;
}

function search_name_tag() {

    var input, filter, table, tr, td, i;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    tasksBody = document.getElementById("table_body");
    tr = tasksBody.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td1 = tr[i].getElementsByTagName("td")[1];
        td2 = tr[i].getElementsByTagName("td")[2];
        if (td1) {
            if (td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }

    }
}